<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
declare(strict_types=1);

namespace Rapa\Core\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Module\ModuleListInterface;

class ModuleVersion extends Field
{
    /**
     * @var string
     */
    protected $_moduleName  = 'Rapa_Core';

    /**
     * @var ModuleListInterface
     */
    protected $_moduleList;

    /**
     * @var string
     */
    protected $_template    = 'Rapa_Core::system/config/module_version.phtml';

    /**
     * AbstractModuleVersion constructor.
     *
     * @param ModuleListInterface $moduleList
     * @param Context $context
     * @param string|null $moduleName
     * @param array $data
     */
    public function __construct(
        ModuleListInterface $moduleList,
        Context $context,
        string $moduleName = null,
        array $data = []
    ) {
        if (!empty($moduleName)) {
            $this->_moduleName = $moduleName;
        }

        $this->_moduleList = $moduleList;
        parent::__construct($context, $data);
    }

    /**
     * Return element html
     *
     * @param  AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Returns module version
     *
     * @return mixed
     */
    public function getModuleVersion()
    {
        return $this->_moduleList->getOne($this->_moduleName)['setup_version'];
    }
}
