<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
declare(strict_types=1);

namespace Rapa\Core\Block\Adminhtml\Config\Form;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\Data\Form\Element\Factory as ElementFactory;

abstract class AbstractMultipleFields extends AbstractFieldArray
{
    /**
     * @var ElementFactory
     */
    protected $_elementFactory;

    /**
     * @var array
     */
    protected $_columnInfo = [];

    /**
     * AbstractMultipleFields constructor.
     *
     * @param Context $context
     * @param ElementFactory $elementFactory
     * @param array $data
     */
    public function __construct(
        Context $context,
        ElementFactory $elementFactory,
        array $data = []
    ) {
        $this->_addAfter = false;
        $this->_elementFactory  = $elementFactory;
        parent::__construct($context, $data);
    }

    /**
     * Add column element information
     *
     * @param string $name
     * @param string $type
     * @param array $options
     * @param array $params
     * @return $this
     */
    public function addColumnInfo(
        string $name,
        string $type,
        array $options,
        array $params
    ) {
        $this->_columnInfo[$name] = [
            'type'      => $type,
            'options'   => $options,
            'params'    => $params
        ];

        return $this;
    }

    /**
     * Returns column elements information
     *
     * @return array
     */
    public function getColumnInfo()
    {
        return $this->_columnInfo;
    }

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        foreach ($this->getColumnInfo() as $name => $info) {
            $this->addColumn($name, $info['params']);
        }
    }

    /**
     * Rendering
     *
     * @param string $columnName
     * @return string
     * @throws \Exception
     */
    public function renderCellTemplate($columnName)
    {
        if (isset($this->_columnInfo[$columnName])) {
            $element = $this->_elementFactory->create($this->_columnInfo[$columnName]['type']);
            $element->setForm(
                $this->getForm()
            )->setName(
                $this->_getCellInputElementName($columnName)
            )->setHtmlId(
                $this->_getCellInputElementId('<%- _id %>', $columnName)
            )->setValues(
                (is_array($this->_columnInfo[$columnName]['options']) ? $this->_columnInfo[$columnName]['options'] : [])
            );
            if (!empty($this->_columnInfo[$columnName]['params']['class'])) {
                $element->setClass($this->_columnInfo[$columnName]['params']['class']);
            }

            return str_replace("\n", '', $element->getElementHtml());
        }

        return parent::renderCellTemplate($columnName);
    }
}
