<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
declare(strict_types=1);

namespace Rapa\Core\Helper;

use Magento\Store\Model\ScopeInterface;

abstract class AbstractHelper extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var array
     */
    protected $templateMap      = [];

    /**
     * Returns config value
     *
     * @param string $path
     * @param mixed|null $defValue
     * @param string|null $scopeType
     * @param string|int|null $scopeCode
     * @return mixed|null
     */
    public function getConfig(
        string $path,
        $defValue = null,
        string $scopeType = null,
        $scopeCode = null
    ) {
        $result = $this->scopeConfig->getValue(
            $path,
            ($scopeType ?? ScopeInterface::SCOPE_STORE),
            $scopeCode
        );

        return (isset($defValue) && empty($result) ? $defValue : $result);
    }
}
