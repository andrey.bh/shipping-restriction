<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
declare(strict_types=1);

namespace Rapa\ShippingRestriction\Helper;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Rapa\Core\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    const CONFIG_PATH_GENERAL       = 'rapa_shipping_restriction/general/';

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var array
     */
    protected $_rules;

    /**
     * Data constructor.
     *
     * @param Context $context
     * @param CustomerSession $customerSession
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession
    ) {
        $this->_customerSession = $customerSession;
        parent::__construct($context);
    }

    /**
     * Returns customer group id
     *
     * @return int
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCustomerGroupId()
    {
        return $this->_customerSession->getCustomerGroupId();
    }

    /**
     * Returns restricted rules
     *
     * @return array
     */
    public function getRules(int $groupId = null)
    {
        if (!isset($this->_rules)) {
            $this->_rules = [];
            $rules = json_decode($this->getConfig(static::CONFIG_PATH_GENERAL . 'rules'), true);
            if (is_array($rules)) {
                foreach ($rules as $rule) {
                    if (is_array($rule['groups']) && is_array($rule['methods'])) {
                        foreach ($rule['groups'] as $group) {
                            if (!isset($this->_rules[$group])) {
                                $this->_rules[$group] = [];
                            }

                            $this->_rules[$group] = array_unique(array_merge($this->_rules[$group], $rule['methods']));
                        }
                    }
                }
            }
        }

        return isset($groupId) ? ($this->_rules[$groupId] ?? []) : $this->_rules;
    }

    /**
     * Checks if module is enabled
     *
     * @return bool
     */
    public function isEnabled()
    {
        return (bool) $this->getConfig(static::CONFIG_PATH_GENERAL . 'enable');
    }

    /**
     * Checks if shipping method is available for current customer
     *
     * @param string $code
     * @return bool
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function isAvailable(string $code)
    {
        return !$this->isEnabled() || !in_array($code, $this->getRules($this->getCustomerGroupId()));
    }
}
