<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
declare(strict_types=1);

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Rapa_ShippingRestriction',
    __DIR__
);
