<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
declare(strict_types=1);

namespace Rapa\ShippingRestriction\Block\Adminhtml\Config;

use Rapa\Core\Block\Adminhtml\Config\Form\AbstractMultipleFields;
use Magento\Backend\Block\Template\Context;
use Magento\Customer\Model\Config\Source\Group\Multiselect as CustomerGroups;
use Magento\Framework\Data\Form\Element\Factory as ElementFactory;
use Magento\Shipping\Model\Config\Source\Allmethods as ShippingMethods;

class Methods extends AbstractMultipleFields
{
    /**
     * @var array
     */
    protected $_customerGroups;

    /**
     * @var array
     */
    protected $_shippingMethods;

    /**
     * Methods constructor.
     *
     * @param Context $context
     * @param CustomerGroups $customerGroups
     * @param ElementFactory $elementFactory
     * @param ShippingMethods $shippingMethods
     * @param array $data
     */
    public function __construct(
        Context $context,
        CustomerGroups $customerGroups,
        ElementFactory $elementFactory,
        ShippingMethods $shippingMethods,
        array $data = []
    ) {
        $this->_customerGroups = array_merge(
            [['value' => '0', 'label' => __('Not Registered')]],
            $customerGroups->toOptionArray()
        );
        $this->_shippingMethods = $shippingMethods->toOptionArray();
        array_shift($this->_shippingMethods);
        parent::__construct($context, $elementFactory, $data);
    }

    /**
     * Prepare to render
     *
     * @return void
     */
    protected function _prepareToRender()
    {
        $this->addColumnInfo(
            'groups',
            'multiselect',
            $this->_customerGroups,
            [
                'label' => __('Customer Groups'),
                'class' => 'validate-select'
            ]
        )->addColumnInfo(
            'methods',
            'multiselect',
            $this->_shippingMethods,
            [
                'label' => __('Restricted Shipping Methods'),
                'class' => 'validate-select'
            ]
        );

        parent::_prepareToRender();
    }
}
