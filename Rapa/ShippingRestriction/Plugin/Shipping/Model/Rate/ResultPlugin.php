<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
declare(strict_types=1);

namespace Rapa\ShippingRestriction\Plugin\Shipping\Model\Rate;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote\Address\RateResult\AbstractResult;
use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Shipping\Model\Rate\Result;
use Rapa\ShippingRestriction\Helper\Data as Helper;

class ResultPlugin
{
    /**
     * @var Helper
     */
    protected $_helper;

    /**
     * ResultPlugin constructor.
     *
     * @param Helper $helper
     */
    public function __construct(
        Helper $helper
    ) {
        $this->_helper = $helper;
    }

    /**
     * Around plugin for append method
     *
     * @param Result $subject
     * @param callable $proceed
     * @param AbstractResult|Result $result
     * @return Result
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function aroundAppend(
        Result $subject,
        callable $proceed,
        $result
    ) {
        if ($result instanceof Method) {
            $methodCode = $result->getCarrier() . '_' . $result->getMethod();
            if (!$this->_helper->isAvailable($methodCode)) {
                return $subject;
            }
        }

        return $proceed($result);
    }
}
