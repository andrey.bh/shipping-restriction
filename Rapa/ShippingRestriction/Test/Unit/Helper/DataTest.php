<?php
/**
 * Copyright © PE Andrey Rapshtynsky. All rights reserved.
 */
declare(strict_types=1);

namespace Rapa\ShippingRestriction\Test\Unit\Helper;

use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\ScopeInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Rapa\ShippingRestriction\Helper\Data as Helper;

class DataTest extends TestCase
{
    /**
     * @var Context|MockObject
     */
    private $context;

    /**
     * @var CustomerSession|MockObject
     */
    private $customerSession;

    /**
     * @var Helper
     */
    private $object;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    protected function setUp()
    {
        // Create ScopeConfig
        $this->scopeConfig = $this->getMockBuilder(ScopeConfigInterface::class)
            ->getMockForAbstractClass();

        $this->context = $this->getMockBuilder(Context::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->context->expects($this->any())
            ->method('getScopeConfig')
            ->willReturn($this->scopeConfig);

        $this->customerSession = $this->getMockBuilder(CustomerSession::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->object = new Helper(
            $this->context,
            $this->customerSession
        );
    }

    /**
     * Sets group id
     *
     * @param int $value
     * @return $this
     */
    protected function setGroupId(int $value)
    {
        $this->customerSession->expects($this->any())
            ->method('getCustomerGroupId')
            ->willReturn($value);

        return $this;
    }

    /**
     * Setting up correct data
     *
     * @return $this
     */
    protected function setConfig($rules = null, $isEnabled = true)
    {
        $this->scopeConfig->expects($this->any())
            ->method('getValue')
            ->willReturnMap([
                [
                    'rapa_shipping_restriction/general/enable',
                    ScopeInterface::SCOPE_STORE,
                    null,
                    $isEnabled
                ],
                [
                    'rapa_shipping_restriction/general/rules',
                    ScopeInterface::SCOPE_STORE,
                    null,
                    json_encode($rules)
                ],
            ]);

        return $this;
    }

    /**
     * Returns predefined correct data
     *
     * @return array
     */
    protected function getCorrectRules()
    {
        return [
            '_012'  => [
                'groups'    => ['0'],
                'methods'   => ['flatrate_flatrate', 'tablerate_bestway', 'freeshipping_freeshipping'],
            ],
            '_123'  => [
                'groups'    => ['1', '3'],
                'methods'   => ['tablerate_bestway', 'ups_01', 'ups_02', 'ups_03'],
            ],
            '_234'  => [
                'groups'    => ['3'],
                'methods'   => ['flatrate_flatrate', 'tablerate_bestway'],
            ],
        ];
    }

    /**
     * Returns predefined incorrect data
     *
     * @return array
     */
    protected function getIncorrectRules()
    {
        return [
            '_012'  => [
                'groups'    => ['0'],
            ],
            '_123'  => [
                'groups'    => ['1', '3'],
                'methods'   => ['tablerate_bestway', 'ups_01', 'ups_02', 'ups_03'],
            ],
            '_234'  => [
                'groups'    => '3',
                'methods'   => ['flatrate_flatrate', 'tablerate_bestway'],
            ],
            '_345'  => [],
        ];
    }

    /**
     * Returns all rules
     */
    public function testGetRulesForGroup()
    {
        $this->setConfig(
            $this->getCorrectRules()
        )->assertEquals(
            [
                0   => ['flatrate_flatrate', 'tablerate_bestway', 'freeshipping_freeshipping'],
                1   => ['tablerate_bestway', 'ups_01', 'ups_02', 'ups_03'],
                3   => ['tablerate_bestway', 'ups_01', 'ups_02', 'ups_03', 'flatrate_flatrate'],
            ],
            $this->object->getRules()
        );
    }

    /**
     * Returns rules for customer group 0 (Not registered)
     */
    public function testGetRulesForGroup0()
    {
        $this->setConfig(
            $this->getCorrectRules()
        )->assertEquals(
            ['flatrate_flatrate', 'tablerate_bestway', 'freeshipping_freeshipping'],
            $this->object->getRules(0)
        );
    }

    /**
     * Returns rules for customer group 1
     */
    public function testGetRulesForGroup1()
    {
        $this->setConfig(
            $this->getCorrectRules()
        )->assertEquals(
            ['tablerate_bestway', 'ups_01', 'ups_02', 'ups_03'],
            $this->object->getRules(1)
        );
    }

    /**
     * Returns rules for customer group 2
     */
    public function testGetRulesForGroup2()
    {
        $this->setConfig(
            $this->getCorrectRules()
        )->assertEquals(
            [],
            $this->object->getRules(2)
        );
    }

    /**
     * Returns rules for customer group 3
     */
    public function testGetRulesForGroup3()
    {
        $this->setConfig(
            $this->getCorrectRules()
        )->assertEquals(
            ['tablerate_bestway', 'ups_01', 'ups_02', 'ups_03', 'flatrate_flatrate'],
            $this->object->getRules(3)
        );
    }

    /**
     * Returns empty rules, when data not set
     */
    public function testGetRulesNull()
    {
        $this->setConfig()
            ->assertEquals(
                [],
                $this->object->getRules()
            );
    }

    /**
     * Returns empty rules, if data not in JSON format
     */
    public function testGetRulesNotJson()
    {
        $this->setConfig('asd')
            ->assertEquals(
                [],
                $this->object->getRules()
            );
    }

    /**
     * Returns rules, when some of data are incorrect
     */
    public function testGetRulesIncorrectSomeData()
    {
        $this->setConfig(
            $this->getIncorrectRules()
        )->assertEquals(
            [
                1   => ['tablerate_bestway', 'ups_01', 'ups_02', 'ups_03'],
                3   => ['tablerate_bestway', 'ups_01', 'ups_02', 'ups_03'],
            ],
            $this->object->getRules()
        );
    }

    /**
     * Checks availability for group 0
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testIsAvailable0()
    {
        $this->setConfig(
            $this->getCorrectRules()
        )->setGroupId(0)
            ->assertEquals(
                false,
                $this->object->isAvailable('flatrate_flatrate')
            );

    }

    /**
     * Checks availability for group 1
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testIsAvailable1()
    {
        $this->setConfig(
            $this->getCorrectRules()
        )->setGroupId(1)
            ->assertEquals(
                true,
                $this->object->isAvailable('flatrate_flatrate')
            );

    }

    /**
     * Checks availability for disabled module
     *
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function testIsAvailableDisabled()
    {
        $this->setConfig(
            $this->getCorrectRules(),
            true
        )->setGroupId(0)
            ->assertEquals(
                false,
                $this->object->isAvailable('flatrate_flatrate')
            );

    }
}
